package com.wow2all.partner.activity;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.wow2all.partner.R;
import com.wow2all.partner.model.User;
import com.wow2all.partner.utils.Identity;
import com.wow2all.partner.utils.SessionManager;
import com.onesignal.OneSignal;


import static com.wow2all.partner.utils.SessionManager.login;


public class FirstActivity extends AppCompatActivity {

    SessionManager sessionManager;
    User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first);

        //show update dialog when re-open the app
        Identity.setUpdateDialog(this, "");

        sessionManager = new SessionManager(FirstActivity.this);
        user = sessionManager.getUserDetails("");
        Thread timer = new Thread() {
            public void run() {
                try {
                    sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    if (sessionManager.getBooleanData(login)) {
                        if (!user.getCategoryid().equalsIgnoreCase("0")) {
                            OneSignal.sendTag("Categoryid", user.getCategoryid());
                            Intent openMainActivity = new Intent(FirstActivity.this, HomeActivity.class);
                            startActivity(openMainActivity);
                            finish();
                        } else {
                            Intent openMainActivity = new Intent(FirstActivity.this, CategoryActivity.class);
                            startActivity(openMainActivity);
                            finish();
                        }

                    } else {
                        Intent openMainActivity = new Intent(FirstActivity.this, LoginActivity.class);
                        startActivity(openMainActivity);
                        finish();
                    }

                }
            }
        };
        timer.start();
    }
}