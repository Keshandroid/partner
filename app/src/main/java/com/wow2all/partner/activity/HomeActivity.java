package com.wow2all.partner.activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.wow2all.partner.R;
import com.wow2all.partner.fragment.CompleteFragment;
import com.wow2all.partner.fragment.EarningFragment;
import com.wow2all.partner.fragment.HomeFragment;
import com.wow2all.partner.fragment.OngoingFragment;
import com.wow2all.partner.fragment.ProfileFragment;
import com.wow2all.partner.utils.Identity;
import com.wow2all.partner.utils.SessionManager;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

public class HomeActivity extends BaseActivity implements BottomNavigationView.OnNavigationItemSelectedListener {

    SessionManager sessionManager;
    BottomNavigationView navigation;

    //update app dialog
    String currentVersion,latestVersion;
    private Dialog popup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        setContentView(R.layout.activity_home);
        sessionManager = new SessionManager(HomeActivity.this);
        navigation = findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(this);
        navigation.setSelectedItemId(R.id.page_1);

        getCurrentVersion();

    }

    private void getCurrentVersion() {
        PackageManager pm = this.getPackageManager();
        PackageInfo pInfo = null;
        try {
            pInfo =  pm.getPackageInfo(this.getPackageName(),0);
        } catch (PackageManager.NameNotFoundException e1) {
            e1.printStackTrace();
        }
        currentVersion = pInfo.versionName;

        new GetLatestVersion().execute();

    }

    private class GetLatestVersion extends AsyncTask<String, String, JSONObject> {

        private ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected JSONObject doInBackground(String... params) {
            try {
//It retrieves the latest version by scraping the content of current version from play store at runtime
                Document doc = Jsoup.connect("https://play.google.com/store/apps/details?id=com.wow2all.partner&hl=en_IN&gl=US").get();
                latestVersion = doc.getElementsByClass("htlgb").get(6).text();
                //printLog(TAG,"=== latest version===" + latestVersion);

            }catch (Exception e){
                e.printStackTrace();

            }

            return new JSONObject();
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {

            if(latestVersion!=null) {

                if (!currentVersion.equalsIgnoreCase(latestVersion)){
                    if(!isFinishing()){
                        //This would help to prevent Error : BinderProxy@45d459c0 is not valid; is your activity running? error

                        Log.d("VERSION_CHECK", "UPDATE THE APP");

                        if(!Identity.getUpdateDialog(HomeActivity.this).equals("update_later")){
                            showUpdateDialog();
                        }
                    }
                } else {
                    Log.d("VERSION_CHECK", "CONTINUE OPEN APP");
                }
            }
            else
                //background.start();
                super.onPostExecute(jsonObject);
        }
    }

    public void showUpdateDialog() {

        try {
            if (!isFinishing()) {
                popup = new Dialog(this, R.style.DialogCustom);
                popup.setContentView(R.layout.dialog_update_app);
                popup.setCanceledOnTouchOutside(false);
                popup.setCancelable(false);
                popup.show();

                TextView txtNoThanks = popup.findViewById(R.id.txtNoThanks);
                Button btnUpdate = popup.findViewById(R.id.btnUpdate);

                txtNoThanks.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Identity.setUpdateDialog(HomeActivity.this, "update_later");
                        popup.dismiss();
                    }
                });

                btnUpdate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        popup.dismiss();

                        try {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.wow2all.partner")));
                        } catch (Exception e) {
                            Toast.makeText(HomeActivity.this, "Unable to Connect Try Again...",Toast.LENGTH_SHORT).show();
                            e.printStackTrace();
                        }
                    }
                });
            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    private boolean loadFragment(Fragment fragment) {
        //switching fragment
        if (fragment != null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment_container, fragment)
                    .commit();
            return true;
        }
        return false;
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Fragment fragment = null;

        switch (item.getItemId()) {
            case R.id.page_1:
                item.setChecked(true);
                loadFragment(new HomeFragment());
                break;
            case R.id.page_2:
                item.setChecked(true);
                loadFragment(new OngoingFragment());
                break;
            case R.id.page_3:
                item.setChecked(true);
                loadFragment(new EarningFragment());
                break;
            case R.id.page_4:
                item.setChecked(true);
                loadFragment(new ProfileFragment());
                break;
            case R.id.page_5:
                item.setChecked(true);
                loadFragment(new CompleteFragment());
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + item.getItemId());
        }

        return loadFragment(fragment);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.top_option, menu);
        return super.onCreateOptionsMenu(menu);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.logout) {

            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(HomeActivity.this);
            alertDialogBuilder.setMessage(getResources().getString(R.string.logoutmessege));
            alertDialogBuilder.setPositiveButton("yes",
                    (arg0, arg1) -> {
                        sessionManager.logoutUser();
                        startActivity(new Intent(HomeActivity.this, LoginActivity.class)
                                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                    });

            alertDialogBuilder.setNegativeButton("No", (dialog, which) -> dialog.cancel());
            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();

        }
        return true;
    }
}