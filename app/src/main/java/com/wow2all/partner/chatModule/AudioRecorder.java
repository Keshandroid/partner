package com.wow2all.partner.chatModule;

import android.app.Activity;
import android.content.pm.PackageManager;
import android.media.MediaRecorder;
import android.os.Build;
import android.os.Environment;
import android.widget.Toast;

import androidx.core.content.ContextCompat;

import java.io.IOException;
import java.util.Random;

import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.RECORD_AUDIO;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class AudioRecorder {
    Activity activity;
    public AudioRecorder(Activity activity){
        this.activity=activity;
    }

    String AudioSavePathInDevice;
    MediaRecorder mediaRecorder;
    Random random;
    String RandomAudioFileName = "ABCDEFGHIJKLMNOP";
    Integer RequestPermissionCode = 1;

    public void startRecording() {
        random = new Random();
        if (checkPermission()) {
            AudioSavePathInDevice = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getAbsolutePath() + "/" +
                    CreateRandomAudioFileName(5) + "AudioRecording.3gp";
            mediaRecorderReady();
            try {
                mediaRecorder.prepare();
                mediaRecorder.start();
            } catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            Toast.makeText(
                activity, "Recording started",
                Toast.LENGTH_LONG
            ).show();
        } else {
            requestPermission();
        }
    }

    public String getAudioPath() {
        if (AudioSavePathInDevice != null)
            return AudioSavePathInDevice;
        else return "";
    }

    public void stopRecording() {
        try {
            mediaRecorder.stop();
            Toast.makeText(activity, "Recording Completed", Toast.LENGTH_LONG).show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    String CreateRandomAudioFileName(Integer string) {
        StringBuilder stringBuilder = new StringBuilder(string);
        int i = 0;
        while (i < string) {
            stringBuilder.append(RandomAudioFileName.indexOf(random.nextInt(RandomAudioFileName.length())));
            i++;
        }
        return stringBuilder.toString();
    }

    private void requestPermission() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            String[] title = {
                    WRITE_EXTERNAL_STORAGE,
                    RECORD_AUDIO
            };
            activity.requestPermissions(title, RequestPermissionCode);
        }else{
            Toast.makeText(activity, "Device not compatible for this feature", Toast.LENGTH_LONG).show();
        }
    }

    private void mediaRecorderReady() {
        mediaRecorder = new MediaRecorder();
        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        mediaRecorder.setAudioEncoder(MediaRecorder.OutputFormat.AMR_NB);
        mediaRecorder.setOutputFile(AudioSavePathInDevice);
    }

    private Boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(activity, WRITE_EXTERNAL_STORAGE);
        int result2 = ContextCompat.checkSelfPermission(activity, READ_EXTERNAL_STORAGE);
        int result1 = ContextCompat.checkSelfPermission(
            activity,
            RECORD_AUDIO
        );
        return result == PackageManager.PERMISSION_GRANTED &&
                result1 == PackageManager.PERMISSION_GRANTED &&
                result2 == PackageManager.PERMISSION_GRANTED;
    }
}