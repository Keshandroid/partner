package com.wow2all.partner.chatModule.objects

import com.wow2all.partner.chatModule.objects.ChatMsgMembersObject

class ChatMsgTempObject {
    var mediaName: Any?=null
    var mediaType: Any?=null
    var messageText: Any?=null
    var messageTime: Any?=null
    var senderId: String = ""
    var usersList:ArrayList<ChatMsgMembersObject>?=null
}